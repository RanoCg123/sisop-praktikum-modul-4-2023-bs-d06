# sisop-praktikum-modul-4-2023-BS-D06
## Anggota
### 5025211119	Gilang Aliefidanto

### 5025211056	I Gusti Agung Ngurah Adhi Sanjaya

### 5025211185	Rano Noumi Sulistyo

## SOAL 1
### Overview Soal 1
Pada soal 1 ini, akan membuat sebuah program .c yang berisi command untuk mendownload file, mengekstrak file, dan mengeluarkan output dari file.csv, lalu akan membuat sebuah dockerfile yang dimana berisi docker image yang akan menjalankan file.c yang sudah di buat. Setelah itu akan membuat docker compose dengan instance sebanyak 5. Lalu akan mengupload hasil dari docker images ke dalam docker hub.

### Penjelasan
Pada point a akan diminta untuk membuat file .c dengan nama storage.c, didalam storage .c terdapat beberapa line code sebagai berikut :


system("kaggle datasets download -d bryanb/fifa-player-stats-database");

Pada line code ini, akan mengunduh file dengan command dari kaggle menggunakan system();
Untuk mengunduh file kaggle ini membutuhkan kaggle.json, akan dijelaskan di section bawah nanti.


system("unzip fifa-player-stats-database.zip");

Pada line code ini, mengekstrak file yang sudah di unduh menggunakan command diatas.


system("awk -F ',' '$3 < 25 && $8 > 85 && $9 /"Manchester City/" {print $2 ,$3 ,$9 ,$5 ,$8, $4}' FIFA23_official_data.csv");

Pada line code ini, akan menampilkan beberapa data colom dari file FIFA23_official_data.csv dengan kondisi pemain berumur dibawah 25 tahun, potensialnya diatas 85, dan bermain selain di club Manchester City.

Selanjutnya akan membuat dockerfile dengan membuat file txt dengan nama "Dockerfile" yang berisi code : 

### Base image
FROM ubuntu:latest

### Install required packages
RUN apt-get update && \
    apt-get install -y \
    wget \
    unzip \
    python3 \
    python3-pip \
    build-essential

RUN pip3 install kaggle

### Set the working directory
WORKDIR /app

### Copy the source code and dataset files to the container
COPY kaggle.json /root/.kaggle/
COPY storage.c .

### Compile and run the C program
RUN gcc storage.c -o storage
CMD ["./storage"]


Pada isi dari Dockerfile.txt ini pertama akan membuat docker images dengan os ubuntu yang versinya latest, lalu akan menginstall beberapa required packages yang diperlukan, lalu akan run pip3 install kaggle untuk menginstall kaggle di dalam images base supaya dapat mendownload file kaggle, lalu set working directorynya menggunakan WORKDIR /app, lalu akan mengcopy kaggle.json yang sudah didownload didalam account kaggle lalu menaruhnya di current directory lalu akan di copy ke dalam /root/.kaggle/ yang ada di dalam docker images yang akan di buat, lalu akan copy storage.c ke dalam docker images, setelah itu docker file akan mengcompile file storage.c yang sudah di copy lalu akan mengjalankan nya langsung di dalam docker images.


docker build -t gungadhisanjaya/modul4 .

Setelah dockerfile.txt di buat, langkah selanjutkan akan mengbuild docke file itu di dalam terminal ubuntu menggunakan command code diatas.


docker run --rm gungadhisanjaya/modul4

Pada command code, berguna untuk mencoba menjalankan docker images yang kita buat apakah berhasil atau tidak.


Langkah selanjutkan kita akan memasukan docker images ke dalam docker hub yang kita punya dengan cara berikut :


docker login

Pertama tama, harus login terlebih dahulu dengan account docker kita (masukan password dan username)


docker push gungadhisanjaya/modul4

Setelah itu kita akan memasukan kedalam repository docker hub kita menggunakan code diatas.

Dan yang terakhir akan membuat docker compose dengan membuat file docker-compose.yaml dengan isi code sebagai berikut :


version: '3'

services:
  barcelona:
    build:
      context: ./Barcelona
      dockerfile: Dockerfile
    deploy:
      replicas: 5

  napoli:
    build:
      context: ./Napoli
      dockerfile: Dockerfile
    deploy:
      replicas: 5


Code diatas akan membuat docker-compose yang akan jalan pada folder Barcelona dan Napoli yang sudah disiapkan sebelum dockercompose dijalankan, dan isi dari tiap folder ini adalah dockerfile, storage.c, kaggle.json, dan docker-compose.yaml. Docker compose ini akan melakujan instance sebanyak 5 dengan code replicas: 5.

Lalu setelah itu, docker-compose.yaml ini akan di jalankan di current deirectory teminal dengan command : 


docker-compose up -d

docker-compose up ini berfungsi untuk menjalankan docker-compose yang sudah dibuat, dan di dalam terminal akan terlihat keterangan jika docker-compose berhasil di buat dan dijalankan.


## SOAL 2
### Penjelasan Umum
Pada soal diminta untuk membuat file ‘germa.c’ dan ‘logmucatatsini.txt’. Fungsi germa.c nantinya akan berfungsi untuk mengunduh file dengan link yang telah ditentukan, kemudian diekstrak, dan dilakukan mount menggunakan FUSE supaya file yang telah diunduh dapat dilakukan beberapa operasi yang berlaku pada soal. Pada soal meminta beberapa hal:
- Apabila terdapat file yang mengandung kata restricted, maka file tersebut tidak dapat di-rename ataupun dihapus.
- Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.
- Apabila terdapat file atau folder yang mengandung kata bypass, maka file atau folder yang terkena aturan restricted dapat menghilangkan kedua aturan tadi. Kata sihir tersebut akan berlaku untuk folder dan file yang ada di dalamnya.
Setelah program dijalankan, maka dilakukan beberapa pengujian:
- Buatlah folder productMagang pada folder /src_data/germaa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germaa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal.
- Ubahlah nama folder dari restricted_list pada folder /src_data/germaa/projects/restricted_list/ menjadi /src_data/germaa/projects/bypass_list/. Kemudian, buat folder projectMagang di dalamnya.
- Karena folder projects menjadi dapat diakses, ubahlah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.
- Terakhir, coba kamu hapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus
Tiap kegiatan percobaan, akan dicatat log ke file logmucatatsini.txt dengan format
```
[status]::DD/MM/YYYY-HH:MM:SS::MKDIR:: [user]-Create directory [folder_name]
[status]::DD/MM/YYYY-HH:MM:SS::RENAME:: [user]-Rename from [old _name] to [new _name]
[status]::DD/MM/YYYY-HH:MM:SS::RMDIR:: [user]-Remove directory [folder_name]
[status]::DD/MM/YYYY-HH:MM:SS::RMFILE:: [user]-Remove file [file_name]

[status] = “SUCCESS” or “FAIL”
```

#### Penjelasan Program (germa.c)
Pada program ini, terdapat beberapa perintah mengunduh file, ekstrak file, dan beberapa perintah untuk mengolah file dan folder. Untuk mengolah file dan folder, tersedia contoh pada modul4 sebagai referensi untuk membuat program untuk membuat folder, mengubah nama file dan folder, dan menghapus file dan folder. Nilai dari mkdir, rename, rmdir, dan unlink akan 0 jika sukses, jika tidak maka nilainya -1. Dengan melihat nilai tersebut, maka akan dibuat beberapa perintah agar file atau folder yang mengandung kata restricted pada path tidak bisa melakukan beberapa hal, jika mengandung kata bypass pada path, maka aturan tersebut hilang.
```
#define FUSE_USE_VERSION 30
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fuse.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <dirent.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>

//WAJIB DIGANTI JIKA PINDAH DIRECTORY
static const char *dirpath = "/home/gilang/sisop-praktikum-modul-4-2023-DS-D06/soal2/nanaxgerma/src_data";
static const char *logpath = "/home/gilang/sisop-praktikum-modul-4-2023-DS-D06/soal2/logmucatatsini.txt";

static const char *restricted = "restricted";
static const char *excepted = "bypass";

static  int  fs_getattr(const char *path, struct stat *stbuf) {
    int res;
    char fpath[1024];

    sprintf(fpath,"%s%s",dirpath,path);
    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1024];

    if(strcmp(path,"/") == 0) {
        path = dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath ,path);

    int res = 0;
    DIR *dir;
    struct dirent *dr;
    (void) offset;
    (void) fi;

    dir = opendir(fpath);

    if(dir == NULL) return -errno;

    while ((dr = readdir(dir)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));

        st.st_ino = dr->d_ino;
        st.st_mode = dr->d_type << 12;
        res = (filler(buf, dr->d_name, &st, 0));
        if(res != 0) break;
    }

    closedir(dir);
    return 0;
}

static int fs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[1024];

    if(strcmp(path,"/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0 ;
    (void) fi;
    fd = open(fpath, O_RDWR);

    if (fd == -1) return -errno;
    res = pread(fd, buf, size, offset);
    if (res == -1) res = -errno;
    close(fd);

    return res;
}



static int fs_rename(const char *oldpath, const char *newpath) {
    int res;
    char *no_create = strstr(oldpath, restricted);
    char *except = strstr(oldpath, excepted);
    char status[10];
    char fnewpath[1024], foldpath[1024];

    if(strcmp(oldpath,"/") == 0){
        oldpath = dirpath;
        sprintf(foldpath,"%s", oldpath);
    }
    else {
        sprintf(foldpath, "%s%s", dirpath, oldpath);
    }

    if(strcmp(newpath,"/") == 0){
        newpath = dirpath;
        sprintf(fnewpath,"%s", newpath);
    }
    else {
        sprintf(fnewpath, "%s%s", dirpath, newpath);
    }

    if(no_create && !except) {
        strcpy(status, "FAIL");
        res = -1;
    }
    else {
        strcpy(status, "SUCCESS");
        res = rename(foldpath, fnewpath);
    }

    char username[20];
    getlogin_r(username, 20);

    time_t time_;
    struct tm* time_now;
    time_ = time(NULL);
    char timestamp[30];
    time_now = localtime(&time_);
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", time_now);

    char command[4096];
    sprintf(command, "echo \"%s::%s::RENAME::%s-Rename from %s to %s\" >> %s", status, timestamp, username, oldpath, newpath, logpath);
    system(command);

    if(res == -1)
        return -errno;
    
    return 0;
}

static int fs_unlink(const char *path) {
    int res;
    char *no_create = strstr(path, restricted);
    char *except = strstr(path, excepted);
    char status[10];
    char fpath[1024];
    if(strcmp(path,"/") == 0) {
        path = dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    if(no_create && !except) {
        strcpy(status, "FAIL");
        res = -1;
    }
    else {
        strcpy(status, "SUCCESS");
        res = unlink(fpath);
    }

    char username[20];
    getlogin_r(username, 20);

    time_t time_;
    struct tm* time_now;
    time_ = time(NULL);
    char timestamp[30];
    time_now = localtime(&time_);
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", time_now);

    char command[2048];
    sprintf(command, "echo \"%s::%s::RMFILE::%s-Remove file %s\" >> %s", status, timestamp, username, path, logpath);
    system(command);

    if(res == -1)
        return -errno;
    
    return 0;
}

static int fs_rmdir(const char *path) {
    int res;
    char *no_create = strstr(path, restricted);
    char *except = strstr(path, excepted);
    char status[10];
    char fpath[1024];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    if(no_create && !except) {
        strcpy(status, "FAIL");
        res = -1;
    }
    else {
        strcpy(status, "SUCCESS");
        res = rmdir(fpath);
    }

    char username[20];
    getlogin_r(username, 20);

    time_t time_;
    struct tm* time_now;
    time_ = time(NULL);
    char timestamp[30];
    time_now = localtime(&time_);
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", time_now);

    char command[2048];
    sprintf(command, "echo \"%s::%s::RMDIR::%s-Remove directory %s\" >> %s", status, timestamp, username, path, logpath);
    system(command);

    if(res == -1)
        return -errno;
    
    return 0;
}

static int fs_mkdir(const char *path, mode_t mode) {
    int res;
    char *no_create = strstr(path, restricted);
    char *except = strstr(path, excepted);
    char status[10];
    char fpath[1024];
    if(strcmp(path,"/") == 0) {
        path = dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    if(no_create && !except) {
        strcpy(status, "FAIL");
        res = -1;
    }
    else {
        strcpy(status, "SUCCESS");
        res = mkdir(fpath, mode);
    }

    char username[20];
    getlogin_r(username, 20);

    time_t time_;
    struct tm* time_now;
    time_ = time(NULL);
    char timestamp[30];
    time_now = localtime(&time_);
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", time_now);

    char command[2048];
    sprintf(command, "echo \"%s::%s::MKDIR::%s-Create directory %s\" >> %s", status, timestamp, username, path, logpath);
    system(command);

    if(res == -1)
        return -errno;
    
    return 0;
}

void unzip() {
    char *command[] = {"unzip", "-q", "uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i", NULL};
    if(fork() == 0) {
        execv("/usr/bin/unzip", command);
    }
    else {
        printf("   OK!\nDeleting zip file");
        wait(NULL);
        remove("uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i");
        printf("   OK!\nDONE!\n");
    }
}

void download() {
    char *command[] = {"wget", "-q", "https://drive.google.com/uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i", NULL};
    if(fork() == 0) {
        execv("/usr/bin/wget", command);
    }
    else {
        printf("Download zip file");
        wait(NULL);
        printf("   OK!\nExtract zip file");
    }
    unzip();
}

static struct fuse_operations fs_oper = {
    .getattr = fs_getattr,
    .readdir = fs_readdir,
    .read = fs_read,
    .mkdir = fs_mkdir,
    .rmdir = fs_rmdir,
    .unlink = fs_unlink,
    .rename = fs_rename,
    
};

int main(int argc, char *argv[]) {
    DIR* dir = opendir(dirpath);
    if(ENOENT == errno) {
        download();
    }
    closedir(dir);

    umask(0);
    return fuse_main(argc, argv, &fs_oper, NULL);
}
```

#### Kendala
Kendala pada saat menjalankan FUSE adalah pada soal dituliskan jika yang dilakukan mount adalah direktori src_data ke direktori dest_data. Namun FUSE tidak bekerja pada src_data, melainkan di dest_data. Ketika berusaha melakukan sesuatu file yang berada di ujung, keluar notifikasi ‘software caused connected abort’ dan Ketika di-refresh keluar notifikasi baru yaitu ‘Transport endpoint is not connected’ dan file tersebut menghilang dari mount. Ketika mencoba kembali ke direktori nanaxgerma dan di-refresh, seluruh folder dan file di dest_data menghilang. Kemudian pada saat mencatat log, username tidak tertulis dengan benar, sehingga terkadang tidak keluar username atau muncul dengan char aneh. Sehingga pada log yang tercetak
```
FAIL::27/05/2023-19:19:14::CREATE:: á őŔU-Create directory /germaa/products/restricted_list/productMagang
FAIL::27/05/2023-19:20:22::CREATE:: á őŔU-Create directory /germaa/projects/restricted_list/productMagang
FAIL::03/06/2023-09:34:28::RENAME::-Rename from /germaa/projects/restricted_list to /germaa/projects/bypass_list
SUCCESS::27/05/2023-19:24:23::CREATE::└-Create directory /germaa/projects/bypass_list/projectMagang
SUCCESS::27/05/2023-20:15:00::CREATE::0 -Create directory /.Trash-1000/files
FAIL::27/05/2023-20:15:01::RMFILE::        -Remove file /others/restrictedFileLama/fileLama1.txt
FAIL::27/05/2023-20:15:16::RMFILE::        -Remove file /others/restrictedFileLama/fileLama2.txt
SUCCESS::03/06/2023-09:29:22::CREATE::p4o╣N -Create directory /.Trash-1000
SUCCESS::03/06/2023-09:29:22::CREATE::pd˝╣N -Create directory /.Trash-1000/info
SUCCESS::03/06/2023-09:29:22::CREATE::p4o╣N -Create directory /.Trash-1000/files
SUCCESS::03/06/2023-09:29:23::RMFILE::-Remove file /others/restrictedFileLama/bypassfileLama1.txt
SUCCESS::03/06/2023-09:29:23::RMFILE::-Remove file /others/restrictedFileLama/bypassfileLama2.txt
```
Seharusnya (termasuk tidak ada error disconnect)
```
FAIL::27/05/2023-19:19:14::CREATE::gilang-Create directory /germaa/products/restricted_list/productMagang
FAIL::27/05/2023-19:20:22::CREATE::gilang-Create directory /germaa/projects/restricted_list/projectMagang
FAIL::03/06/2023-09:34:28::RENAME::gilang-Rename from /germaa/projects/restricted_list to /germaa/projects/bypass_list
SUCCESS::27/05/2023-19:24:23::CREATE::gilang-Create directory /germaa/projects/bypass_list/projectMagang
FAIL::27/05/2023-19:27:02::RENAME::gilang-Rename directory /germaa/projects/bypass_list/restrictedFilePenting/ourProject.txt to /germaa/projects/bypass_list/restrictedFilePenting/waduh.txt
SUCCESS::27/05/2023-20:15:00::CREATE::gilang-Create directory /.Trash-1000/files
FAIL::27/05/2023-20:15:01::RMFILE::gilang-Remove file /others/restrictedFileLama/fileLama1.txt
FAIL::27/05/2023-20:15:16::RMFILE::gilang-Remove file /others/restrictedFileLama/fileLama2.txt
SUCCESS::03/06/2023-09:29:22::CREATE::gilang-Create directory /.Trash-1000
SUCCESS::03/06/2023-09:29:22::CREATE::gilang-Create directory /.Trash-1000/info
SUCCESS::03/06/2023-09:29:22::CREATE::gilang-Create directory /.Trash-1000/files
SUCCESS::03/06/2023-09:29:23::RMFILE::gilang-Remove file /others/restrictedFileLama/bypassFileLama1.txt
SUCCESS::03/06/2023-09:29:23::RMFILE::gilang-Remove file /others/restrictedFileLama/bypassFileLama2.txt
```
Kendala lain pada poin B dituliskan jika rename folder restricted_list menjadi bypass_list (log baris 3), namun hal tersebut tidak dapat dilakukan karena bertentangan dengan aturan kedua yaitu "Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus."

## SOAL 5

Pada soal kelima kita perlu membuat file c yang akan nge mount fuse dalam folder rahasia yang dapat di download dalam link google drive. setelah itu buat sistem registrasi menggunakan Md5 hash. dan jika bisa login rename semua file dalam folder rahasia, buat file txt berisi tree folder rahasia, dan file berisi banyak extensionnya.

### Download file
Kita akan menggunakan system() untuk mendownload file dari link. lalu kita unzip, dan kita remove zipnya
```
void dl_zp_rhs(){
	char* dl = "wget -O rahasia.zip 'https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes'" ;
	system(dl);
    sleep(5);
	system("unzip rahasia.zip");
	system("rm rahasia.zip");
}
```
### Registrasi system
Sistem registrasi dilakukan dengan menggunakan sistem file usr. Dalam eksekusi file kita perlu menginput mode -l atau -r. masing masing untuk login atau registrasi. 
Dalam mode registrasi kita akan membuat file usr.txt dan menginput username dan password. dimana password tersebut lalu akan di hash menggunakan EVP dan dirubah menjadi md5. Setelah diubah di print kedalam usr.txt.
Dalam mode login, kita akn input username dan pasword. lalu kita scan usr file jika ada nama dan password yang sama. jika sama variable global confirmed akan menjadi 1 dan menandakan autentifikasi.

### Rename, result dan extensions
Setelah mendownload dan login berhasil, kita akan melakukan 3 hal, rename folder rahasia, buat file txt berisi tree filesystem. dan mengecek banyak folder dan extensions filenya. 
Kita akan merename dengan menggunakan rekursif rename. kita buka directory rahasia dan mescan isinya jika mendeteksi direktori kita rename direktorinya sesuai yang diminta, untuk kasus ini folder_d06 dan lakukan fungsi lagi. jika mendeteksi file biasa akan merename sesuai yang diminta yaitu d06_file.ext.
Untuk result dan extensions memiliki sistem yang sama, dalam result hanya akan meprint nama dile dengan tambahan |-- untuk tipe tree. sementara untuk file extensions terdapat array extensions yang berisi semua macam extensions yang ada dalam folder rahasai dan countf yang menghitung folder. 

### Docker compose
Untuk menyambungkan file rahasia yang telah dimount dengan path yang diinginkan kita memerlukan docker-compose.yml yang berisi spesifikasi path, version dan image yang diminta
```
version: "3"
services:
  myapp:
      image: rahasia_di_docker_d06
      command: tail -f /dev/null
      volumes:
      - /home/rano/sisop-praktikum-modul-4-2023-bs-d06/soal5/rahasia:/usr/share/rahasia

```
### CODE
```
#define FUSE_USE_VERSION 30
#include <stdio.h>
#include <fuse.h>
// #include <fuse_opt.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <openssl/evp.h>
#include <openssl/md5.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/time.h>
#include <dirent.h>
static int confirmed =0;
static const char *prefix = "d06";
static const char *dirpath = "/home/rano/sisop-praktikum-modul-4-2023-bs-d06/soal5/rahasia";
static const char *usrpath = "/home/rano/sisop-praktikum-modul-4-2023-bs-d06/soal5/usr.txt";
static const char *extpath = "/home/rano/sisop-praktikum-modul-4-2023-bs-d06/soal5/extensions.txt";
static const char *outputPath = "/home/rano/sisop-praktikum-modul-4-2023-bs-d06/soal5/result.txt"; 
char* md5_hash(const char *input) {
    EVP_MD_CTX *mdctx;
    const EVP_MD *md;
    unsigned char digest[MD5_DIGEST_LENGTH];
    mdctx = EVP_MD_CTX_new();
    md = EVP_md5();
    EVP_DigestInit_ex(mdctx, md, NULL);
    EVP_DigestUpdate(mdctx, input, strlen(input));
    EVP_DigestFinal_ex(mdctx, digest, NULL);
    EVP_MD_CTX_free(mdctx);
    char* output = (char*)malloc(MD5_DIGEST_LENGTH * 2 + 1);
    for (unsigned int i = 0; i < MD5_DIGEST_LENGTH; i++) {
        sprintf(&output[i * 2], "%02x", (unsigned int)digest[i]);
    }
    return output;
}
void dl_zp_rhs(){
	//somehow hanya bisa mendownload 2,2 kb ??? 
	char* dl = "wget -O rahasia.zip 'https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes'" ;
	system(dl);
    sleep(5);
	system("unzip rahasia.zip");
	system("rm rahasia.zip");
}
static int reg() {
    char usnm[100];
    char pswd[100];

    printf("Username: ");
    scanf("%s", usnm);
    printf("Password: ");
    scanf("%s", pswd);
    char* hashedpswd = md5_hash(pswd);
    // Menulis username dan hashed password ke dalam file users.txt
    FILE *usr = fopen(usrpath, "a");
    if (usr == NULL) {
        printf("Error: Cannot open user file.\n");
        return -1;
    }
    int exist=0;
    char line[256];
    while (fgets(line, sizeof(line), usr)) {
        char stored_usnm[100];
        char stored_pswd[100];
        sscanf(line, "%[^:]:%s", stored_usnm, stored_pswd);

    
        if (strcmp(usnm, stored_usnm) == 0) {
            exist = 1;
            break;
        }
    }
    if(!exist){
    fprintf(usr, "%s:%s\n", usnm, hashedpswd);}
    else {
        printf("Username already exist");
    }
    fclose(usr);

    printf("Registration successful.\n");
    return 0;
}
static int login() {
    char usnm[100];
    char pswd[100];
    printf("username: ");
    scanf("%s", usnm);
    printf("Password: ");
    scanf("%s", pswd);
    FILE *usr = fopen(usrpath, "r");
    if (usr == NULL) {
        printf("Error: Cannot open user file.\n");
        return -1;
    }

    char line[256];
    confirmed = 0;
    while (fgets(line, sizeof(line), usr)) {
        char stored_usnm[100];
        char stored_pswd[100];
        sscanf(line, "%[^:]:%s", stored_usnm, stored_pswd);

    
        if (strcmp(usnm, stored_usnm) == 0) {
            confirmed = 1;
            break;
        }
    }

    fclose(usr);

    if (confirmed) {
        printf("Login successful.\n");
        return 1;
    } else {
        printf("Invalid username or password.\n");
        return -1;
    }
}
void listDirectory(const char *path, FILE *file, int level) {
    DIR *dir = opendir(path);
    if (dir == NULL) {
        fprintf(stderr, "Error opening directory: %s\n", path);
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            continue;

        for (int i = 0; i < level; i++) {
            fprintf(file, "|   ");
        }

        fprintf(file, "|-- %s\n", entry->d_name);

        if (entry->d_type == DT_DIR) {
            char newPath[1024];
            snprintf(newPath, sizeof(newPath), "%s/%s", path, entry->d_name);
            listDirectory(newPath, file, level + 1);
        }
    }

    closedir(dir);
}
void renameDirectory(const char *path, const char *prefix) {
    DIR *dir = opendir(path);
    if (dir == NULL) {
        printf("Failed to open directory: %s\n", path);
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        // Skip current and parent directory entries
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            continue;

        char oldPath[256];
        snprintf(oldPath, sizeof(oldPath), "%s/%s", path, entry->d_name);

        char newPath[256];
        if (entry->d_type == DT_DIR) {
            snprintf(newPath, sizeof(newPath), "%s/%s_%s", path, entry->d_name, prefix);
            renameDirectory(oldPath, prefix);
        } else {
            snprintf(newPath, sizeof(newPath), "%s/d06_%s", path, entry->d_name);
        }

        if (rename(oldPath, newPath) != 0) {
            printf("Failed to rename: %s\n", entry->d_name);
        }
    }

    closedir(dir);
}
static const char *extensions[] = {".gif", ".mp3", ".pdf", ".png", ".jpg", ".txt", ".docx"};
static const int num_extensions = sizeof(extensions) / sizeof(extensions[0]);

static void count_files_recursive(const char *path,int *count, int *countf) {
    DIR *dp = opendir(path);
    if (dp == NULL) {
        printf("Error: Cannot open folder %s.\n", path);
        return;
    }

    struct dirent *de;
    while ((de = readdir(dp)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;
        char file_path[256];
        sprintf(file_path, "%s/%s", path, de->d_name);

        if (de->d_type == DT_DIR) { // Memeriksa apakah entri adalah direktori
            (*countf)++;
            count_files_recursive(file_path,count, &countf);
        } else if (de->d_type == DT_REG) { // Memeriksa apakah entri adalah file
            char *filename = de->d_name;
            char *extension = strrchr(filename, '.'); // Mengambil ekstensi file dengan mencari titik terakhir

            if (extension != NULL) {
                for (int i = 0; i < num_extensions; i++) {
                    if (strcmp(extension, extensions[i]) == 0) {
                        count[i]++;
                        break;
                    }
                }
            }
        }
    }

    closedir(dp);
}

// Fungsi untuk menghitung jumlah file berdasarkan ekstensi
static void count_files_by_extension() {
    int count[num_extensions];
    memset(count, 0, sizeof(count)); // Inisialisasi semua elemen count menjadi 0
    int countf = 0;
    count_files_recursive("./rahasia",count, &countf);

    // Menyimpan hasil perhitungan ke dalam file extension.txt
    FILE *extension_file = fopen(extpath, "w");
    if (extension_file == NULL) {
        printf("Error: Cannot create extension file.\n");
        return;
    }
    fprintf(extension_file, "folder: %d\n", countf);
    for (int i = 0; i < num_extensions; i++) {
        fprintf(extension_file, "%s: %d\n", extensions[i], count[i]);
    }

    fclose(extension_file);

    printf("File count by extension saved to extension.txt.\n");
}

static int rhs_getattr(const char *path, struct stat *stbuf)
{
    
    int res;
    res = lstat(path, stbuf);

    if (res == -1) return -errno;
    return 0;

}

static int rhs_rename(const char *from, const char *to)
{
    if (!confirmed) {
        printf("Please log in first.\n");
        return -1;
    }

    // Extract the folder or file name from the 'from' path
    char name[256];
    sscanf(from, "/%[^/]", name);

    // Extract the group code from the 'to' path
    char group_code[256];
    sscanf(to, "/%[^_]", group_code);

    // Construct the new folder or file name with the desired format
    char new_name[256];
    sprintf(new_name, "/%s_%s", group_code, name);

    // Construct the old and new paths
    char old_path[256];
    sprintf(old_path, "%s%s", dirpath, from);

    char new_path[256];
    sprintf(new_path, "%s%s", dirpath, to); // Use 'to' directly

    struct stat st;
    stat(old_path, &st);
    int is_directory = S_ISDIR(st.st_mode);

    int res;
    if (is_directory) {
        res = rename(old_path, new_path);
        if (res == -1)
            return -errno;

        // Rename folder recursively
        DIR *dp = opendir(new_path); // Use 'new_path'
        if (dp == NULL)
            return -errno;

        struct dirent *de;
        while ((de = readdir(dp)) != NULL) {
            if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
                continue;

            char old_sub_path[256];
            sprintf(old_sub_path, "%s/%s", old_path, de->d_name);

            char new_sub_path[256];
            sprintf(new_sub_path, "%s/%s", new_path, de->d_name);

            res = rhs_rename(old_sub_path, new_sub_path);
            if (res == -1)
                return -errno;
        }

        closedir(dp);
    } else {
        // Create the parent directory of the new path if it doesn't exist
        char parent_dir[256];
        strncpy(parent_dir, new_path, sizeof(parent_dir));
        parent_dir[sizeof(parent_dir) - 1] = '\0';
        char *parent = dirname(parent_dir);
        mkdir(parent, 0755);

        res = rename(old_path, new_path);
        if (res == -1)
            return -errno;
    }

    // Collect the paths of renamed files/folders
    FILE *result_file = fopen("result.txt", "a");
    if (result_file == NULL) {
        printf("Error: Cannot open the result file.\n");
        return -1;
    }
    fprintf(result_file, "%s -> %s\n", old_path, new_path);
    fclose(result_file);

    return 0;
}


static int rhs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    if (!confirmed) {
        printf("Please login first.\n");
        return -1;
    }

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(path);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if(filler(buf, de->d_name, &st, 0)) break;
    }
    closedir(dp);
    return 0;


}

static int rhs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    if (!confirmed) {
        printf("Please login first.\n");
        return -1;
    }
int fd;
    int res;
    (void) fi;

    fd = open(path, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;

}

static struct fuse_operations rhs_operations = {
    .getattr = rhs_getattr,
    .readdir = rhs_readdir,
    .read = rhs_read,
    .rename = rhs_rename,
};


int main(int argc, char *argv[]) {
    dl_zp_rhs();
    umask(0);
    if (strcmp(argv[2], "-r") == 0) {
        printf("Registering new user...\n");
        reg();
        return 0;
    } else if (strcmp(argv[2], "-l") == 0) {
        printf("Please Log in\n");
        if(login()== 1){
        renameDirectory(dirpath, prefix);
        FILE *outputFile = fopen(outputPath, "w");
        if (outputFile == NULL) {
            fprintf(stderr, "Error opening output file: %s\n", outputPath);
            return 1;
        }
        listDirectory(dirpath, outputFile, 0);
        fclose(outputFile);
        count_files_by_extension();
        return 0;
        }
        } else {
        printf("Invalid argument. Usage: %s <mount-point> <-r|-l>\n", argv[0]);
        return -1;
    }
    return  fuse_main(argc, argv, &rhs_operations, NULL);
}
```