#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <sys/wait.h>

#define filezip "fifa-player-stats-database.zip"
#define MAX_LINE_LENGTH 1000

int main()
{
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip");
    system("awk -F ',' '$3 < 25 && $8 > 85 {print $2 ,$3 ,$9 ,$5 ,$8, $4}' FIFA23_official_data.csv");

    return 0;
}