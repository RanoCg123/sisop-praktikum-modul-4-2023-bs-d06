#define FUSE_USE_VERSION 30
#include <stdio.h>
#include <fuse.h>
// #include <fuse_opt.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <openssl/evp.h>
#include <openssl/md5.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/time.h>
#include <dirent.h>
static int confirmed =0;
static const char *prefix = "d06";
static const char *dirpath = "/home/rano/sisop-praktikum-modul-4-2023-bs-d06/soal5/rahasia";
static const char *usrpath = "/home/rano/sisop-praktikum-modul-4-2023-bs-d06/soal5/usr.txt";
static const char *extpath = "/home/rano/sisop-praktikum-modul-4-2023-bs-d06/soal5/extensions.txt";
static const char *outputPath = "/home/rano/sisop-praktikum-modul-4-2023-bs-d06/soal5/result.txt"; 
char* md5_hash(const char *input) {
    EVP_MD_CTX *mdctx;
    const EVP_MD *md;
    unsigned char digest[MD5_DIGEST_LENGTH];
    mdctx = EVP_MD_CTX_new();
    md = EVP_md5();
    EVP_DigestInit_ex(mdctx, md, NULL);
    EVP_DigestUpdate(mdctx, input, strlen(input));
    EVP_DigestFinal_ex(mdctx, digest, NULL);
    EVP_MD_CTX_free(mdctx);
    char* output = (char*)malloc(MD5_DIGEST_LENGTH * 2 + 1);
    for (unsigned int i = 0; i < MD5_DIGEST_LENGTH; i++) {
        sprintf(&output[i * 2], "%02x", (unsigned int)digest[i]);
    }
    return output;
}
void dl_zp_rhs(){
	//somehow hanya bisa mendownload 2,2 kb ??? 
	char* dl = "wget -O rahasia.zip 'https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes'" ;
	system(dl);
    sleep(5);
	system("unzip rahasia.zip");
	system("rm rahasia.zip");
}
static int reg() {
    char usnm[100];
    char pswd[100];

    printf("Username: ");
    scanf("%s", usnm);
    printf("Password: ");
    scanf("%s", pswd);
    char* hashedpswd = md5_hash(pswd);
    // Menulis username dan hashed password ke dalam file users.txt
    FILE *usr = fopen(usrpath, "a");
    if (usr == NULL) {
        printf("Error: Cannot open user file.\n");
        return -1;
    }
    int exist=0;
    char line[256];
    while (fgets(line, sizeof(line), usr)) {
        char stored_usnm[100];
        char stored_pswd[100];
        sscanf(line, "%[^:]:%s", stored_usnm, stored_pswd);

    
        if (strcmp(usnm, stored_usnm) == 0) {
            exist = 1;
            break;
        }
    }
    if(!exist){
    fprintf(usr, "%s:%s\n", usnm, hashedpswd);}
    else {
        printf("Username already exist");
    }
    fclose(usr);

    printf("Registration successful.\n");
    return 0;
}
static int login() {
    char usnm[100];
    char pswd[100];
    printf("username: ");
    scanf("%s", usnm);
    printf("Password: ");
    scanf("%s", pswd);
    FILE *usr = fopen(usrpath, "r");
    if (usr == NULL) {
        printf("Error: Cannot open user file.\n");
        return -1;
    }

    char line[256];
    confirmed = 0;
    while (fgets(line, sizeof(line), usr)) {
        char stored_usnm[100];
        char stored_pswd[100];
        sscanf(line, "%[^:]:%s", stored_usnm, stored_pswd);

    
        if (strcmp(usnm, stored_usnm) == 0) {
            confirmed = 1;
            break;
        }
    }

    fclose(usr);

    if (confirmed) {
        printf("Login successful.\n");
        return 1;
    } else {
        printf("Invalid username or password.\n");
        return -1;
    }
}
void listDirectory(const char *path, FILE *file, int level) {
    DIR *dir = opendir(path);
    if (dir == NULL) {
        fprintf(stderr, "Error opening directory: %s\n", path);
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            continue;

        for (int i = 0; i < level; i++) {
            fprintf(file, "|   ");
        }

        fprintf(file, "|-- %s\n", entry->d_name);

        if (entry->d_type == DT_DIR) {
            char newPath[1024];
            snprintf(newPath, sizeof(newPath), "%s/%s", path, entry->d_name);
            listDirectory(newPath, file, level + 1);
        }
    }

    closedir(dir);
}
void renameDirectory(const char *path, const char *prefix) {
    DIR *dir = opendir(path);
    if (dir == NULL) {
        printf("Failed to open directory: %s\n", path);
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        // Skip current and parent directory entries
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            continue;

        char oldPath[256];
        snprintf(oldPath, sizeof(oldPath), "%s/%s", path, entry->d_name);

        char newPath[256];
        if (entry->d_type == DT_DIR) {
            snprintf(newPath, sizeof(newPath), "%s/%s_%s", path, entry->d_name, prefix);
            renameDirectory(oldPath, prefix);
        } else {
            snprintf(newPath, sizeof(newPath), "%s/d06_%s", path, entry->d_name);
        }

        if (rename(oldPath, newPath) != 0) {
            printf("Failed to rename: %s\n", entry->d_name);
        }
    }

    closedir(dir);
}
static const char *extensions[] = {".gif", ".mp3", ".pdf", ".png", ".jpg", ".txt", ".docx"};
static const int num_extensions = sizeof(extensions) / sizeof(extensions[0]);

static void count_files_recursive(const char *path,int *count, int *countf) {
    DIR *dp = opendir(path);
    if (dp == NULL) {
        printf("Error: Cannot open folder %s.\n", path);
        return;
    }

    struct dirent *de;
    while ((de = readdir(dp)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;
        char file_path[256];
        sprintf(file_path, "%s/%s", path, de->d_name);

        if (de->d_type == DT_DIR) { // Memeriksa apakah entri adalah direktori
            (*countf)++;
            count_files_recursive(file_path,count, &countf);
        } else if (de->d_type == DT_REG) { // Memeriksa apakah entri adalah file
            char *filename = de->d_name;
            char *extension = strrchr(filename, '.'); // Mengambil ekstensi file dengan mencari titik terakhir

            if (extension != NULL) {
                for (int i = 0; i < num_extensions; i++) {
                    if (strcmp(extension, extensions[i]) == 0) {
                        count[i]++;
                        break;
                    }
                }
            }
        }
    }

    closedir(dp);
}

// Fungsi untuk menghitung jumlah file berdasarkan ekstensi
static void count_files_by_extension() {
    int count[num_extensions];
    memset(count, 0, sizeof(count)); // Inisialisasi semua elemen count menjadi 0
    int countf = 0;
    count_files_recursive("./rahasia",count, &countf);

    // Menyimpan hasil perhitungan ke dalam file extension.txt
    FILE *extension_file = fopen(extpath, "w");
    if (extension_file == NULL) {
        printf("Error: Cannot create extension file.\n");
        return;
    }
    fprintf(extension_file, "folder: %d\n", countf);
    for (int i = 0; i < num_extensions; i++) {
        fprintf(extension_file, "%s: %d\n", extensions[i], count[i]);
    }

    fclose(extension_file);

    printf("File count by extension saved to extension.txt.\n");
}

static int rhs_getattr(const char *path, struct stat *stbuf)
{
    
    int res;
    res = lstat(path, stbuf);

    if (res == -1) return -errno;
    return 0;

}

static int rhs_rename(const char *from, const char *to)
{
    if (!confirmed) {
        printf("Please log in first.\n");
        return -1;
    }

    // Extract the folder or file name from the 'from' path
    char name[256];
    sscanf(from, "/%[^/]", name);

    // Extract the group code from the 'to' path
    char group_code[256];
    sscanf(to, "/%[^_]", group_code);

    // Construct the new folder or file name with the desired format
    char new_name[256];
    sprintf(new_name, "/%s_%s", group_code, name);

    // Construct the old and new paths
    char old_path[256];
    sprintf(old_path, "%s%s", dirpath, from);

    char new_path[256];
    sprintf(new_path, "%s%s", dirpath, to); // Use 'to' directly

    struct stat st;
    stat(old_path, &st);
    int is_directory = S_ISDIR(st.st_mode);

    int res;
    if (is_directory) {
        res = rename(old_path, new_path);
        if (res == -1)
            return -errno;

        // Rename folder recursively
        DIR *dp = opendir(new_path); // Use 'new_path'
        if (dp == NULL)
            return -errno;

        struct dirent *de;
        while ((de = readdir(dp)) != NULL) {
            if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
                continue;

            char old_sub_path[256];
            sprintf(old_sub_path, "%s/%s", old_path, de->d_name);

            char new_sub_path[256];
            sprintf(new_sub_path, "%s/%s", new_path, de->d_name);

            res = rhs_rename(old_sub_path, new_sub_path);
            if (res == -1)
                return -errno;
        }

        closedir(dp);
    } else {
        // Create the parent directory of the new path if it doesn't exist
        char parent_dir[256];
        strncpy(parent_dir, new_path, sizeof(parent_dir));
        parent_dir[sizeof(parent_dir) - 1] = '\0';
        char *parent = dirname(parent_dir);
        mkdir(parent, 0755);

        res = rename(old_path, new_path);
        if (res == -1)
            return -errno;
    }

    // Collect the paths of renamed files/folders
    FILE *result_file = fopen("result.txt", "a");
    if (result_file == NULL) {
        printf("Error: Cannot open the result file.\n");
        return -1;
    }
    fprintf(result_file, "%s -> %s\n", old_path, new_path);
    fclose(result_file);

    return 0;
}


static int rhs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    if (!confirmed) {
        printf("Please login first.\n");
        return -1;
    }

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(path);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if(filler(buf, de->d_name, &st, 0)) break;
    }
    closedir(dp);
    return 0;


}

static int rhs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    if (!confirmed) {
        printf("Please login first.\n");
        return -1;
    }
int fd;
    int res;
    (void) fi;

    fd = open(path, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;

}

static struct fuse_operations rhs_operations = {
    .getattr = rhs_getattr,
    .readdir = rhs_readdir,
    .read = rhs_read,
    .rename = rhs_rename,
};


int main(int argc, char *argv[]) {
    dl_zp_rhs();
    umask(0);
    if (strcmp(argv[2], "-r") == 0) {
        printf("Registering new user...\n");
        reg();
        return 0;
    } else if (strcmp(argv[2], "-l") == 0) {
        printf("Please Log in\n");
        if(login()== 1){
        renameDirectory(dirpath, prefix);
        FILE *outputFile = fopen(outputPath, "w");
        if (outputFile == NULL) {
            fprintf(stderr, "Error opening output file: %s\n", outputPath);
            return 1;
        }
        listDirectory(dirpath, outputFile, 0);
        fclose(outputFile);
        count_files_by_extension();
        return 0;
        }
        } else {
        printf("Invalid argument. Usage: %s <mount-point> <-r|-l>\n", argv[0]);
        return -1;
    }
    return  fuse_main(argc, argv, &rhs_operations, NULL);
}