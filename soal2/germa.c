#define FUSE_USE_VERSION 30
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fuse.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <dirent.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>

//WAJIB DIGANTI JIKA PINDAH DIRECTORY
static const char *dirpath = "/home/gilang/sisop-praktikum-modul-4-2023-DS-D06/soal2/nanaxgerma/src_data";
static const char *logpath = "/home/gilang/sisop-praktikum-modul-4-2023-DS-D06/soal2/logmucatatsini.txt";

static const char *restricted = "restricted";
static const char *except = "bypass";

static  int  fs_getattr(const char *path, struct stat *stbuf) {
    int res;
    char fpath[1024];

    sprintf(fpath,"%s%s",dirpath,path);
    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1024];

    if(strcmp(path,"/") == 0) {
        path = dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath ,path);

    int res = 0;
    DIR *dir;
    struct dirent *dr;
    (void) offset;
    (void) fi;

    dir = opendir(fpath);

    if(dir == NULL) return -errno;

    while ((dr = readdir(dir)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));

        st.st_ino = dr->d_ino;
        st.st_mode = dr->d_type << 12;
        res = (filler(buf, dr->d_name, &st, 0));
        if(res != 0) break;
    }

    closedir(dir);
    return 0;
}

static int fs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[1024];

    if(strcmp(path,"/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0 ;
    (void) fi;
    fd = open(fpath, O_RDWR);

    if (fd == -1) return -errno;
    res = pread(fd, buf, size, offset);
    if (res == -1) res = -errno;
    close(fd);

    return res;
}



static int fs_rename(const char *oldpath, const char *newpath) {
    int res;
    char *no_create = strstr(oldpath, restricted);
    char *except = strstr(oldpath, except);
    char status[10];
    char fnewpath[1024], foldpath[1024];

    if(strcmp(oldpath,"/") == 0){
        oldpath = dirpath;
        sprintf(foldpath,"%s", oldpath);
    }
    else {
        sprintf(foldpath, "%s%s", dirpath, oldpath);
    }

    if(strcmp(newpath,"/") == 0){
        newpath = dirpath;
        sprintf(fnewpath,"%s", newpath);
    }
    else {
        sprintf(fnewpath, "%s%s", dirpath, newpath);
    }

    if(no_create && !except) {
        strcpy(status, "FAIL");
        res = -1;
    }
    else {
        strcpy(status, "SUCCESS");
        res = rename(foldpath, fnewpath);
    }

    char username[20];
    getlogin_r(username, 20);

    time_t time_;
    struct tm* time_now;
    time_ = time(NULL);
    char timestamp[30];
    time_now = localtime(&time_);
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", time_now);

    char command[4096];
    sprintf(command, "echo \"%s::%s::RENAME::%s-Rename from %s to %s\" >> %s", status, timestamp, username, oldpath, newpath, logpath);
    system(command);

    if(res == -1)
        return -errno;
    
    return 0;
}

static int fs_unlink(const char *path) {
    int res;
    char *no_create = strstr(path, restricted);
    char *except = strstr(path, except);
    char status[10];
    char fpath[1024];
    if(strcmp(path,"/") == 0) {
        path = dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    if(no_create && !except) {
        strcpy(status, "FAIL");
        res = -1;
    }
    else {
        strcpy(status, "SUCCESS");
        res = unlink(fpath);
    }

    char username[20];
    getlogin_r(username, 20);

    time_t time_;
    struct tm* time_now;
    time_ = time(NULL);
    char timestamp[30];
    time_now = localtime(&time_);
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", time_now);

    char command[2048];
    sprintf(command, "echo \"%s::%s::REMOVE::%s-Remove directory %s\" >> %s", status, timestamp, username, path, logpath);
    system(command);

    if(res == -1)
        return -errno;
    
    return 0;
}

static int fs_rmdir(const char *path) {
    int res;
    char *no_create = strstr(path, restricted);
    char *except = strstr(path, except);
    char status[10];
    char fpath[1024];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    if(no_create && !except) {
        strcpy(status, "FAIL");
        res = -1;
    }
    else {
        strcpy(status, "SUCCESS");
        res = rmdir(fpath);
    }

    char username[20];
    getlogin_r(username, 20);

    time_t time_;
    struct tm* time_now;
    time_ = time(NULL);
    char timestamp[30];
    time_now = localtime(&time_);
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", time_now);

    char command[2048];
    sprintf(command, "echo \"%s::%s::REMOVE::%s-Remove file %s\" >> %s", status, timestamp, username, path, logpath);
    system(command);

    if(res == -1)
        return -errno;
    
    return 0;
}

static int fs_mkdir(const char *path, mode_t mode) {
    int res;
    char *no_create = strstr(path, restricted);
    char *except = strstr(path, except);
    char status[10];
    char fpath[1024];
    if(strcmp(path,"/") == 0) {
        path = dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    if(no_create && !except) {
        strcpy(status, "FAIL");
        res = -1;
    }
    else {
        strcpy(status, "SUCCESS");
        res = mkdir(fpath, mode);
    }

    char username[20];
    getlogin_r(username, 20);

    time_t time_;
    struct tm* time_now;
    time_ = time(NULL);
    char timestamp[30];
    time_now = localtime(&time_);
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", time_now);

    char command[2048];
    sprintf(command, "echo \"%s::%s::CREATE::%s-Create directory %s\" >> %s", status, timestamp, username, path, logpath);
    system(command);

    if(res == -1)
        return -errno;
    
    return 0;
}

void unzip() {
    char *command[] = {"unzip", "-q", "uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i", NULL};
    if(fork() == 0) {
        execv("/usr/bin/unzip", command);
    }
    else {
        printf("   OK!\nDeleting zip file");
        wait(NULL);
        remove("uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i");
        printf("   OK!\nDONE!\n");
    }
}

void download() {
    char *command[] = {"wget", "-q", "https://drive.google.com/uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i", NULL};
    if(fork() == 0) {
        execv("/usr/bin/wget", command);
    }
    else {
        printf("Download zip file");
        wait(NULL);
        printf("   OK!\nExtract zip file");
    }
    unzip();
}

static struct fuse_operations fs_oper = {
    .getattr = fs_getattr,
    .readdir = fs_readdir,
    .read = fs_read,
    .mkdir = fs_mkdir,
    .rmdir = fs_rmdir,
    .unlink = fs_unlink,
    .rename = fs_rename,
    
};

int main(int argc, char *argv[]) {
    DIR* dir = opendir(dirpath);
    if(ENOENT == errno) {
        download();
    }
    closedir(dir);

    umask(0);
    return fuse_main(argc, argv, &fs_oper, NULL);
}
